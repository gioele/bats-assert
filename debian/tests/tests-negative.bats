#!/usr/bin/env bats

bats_load_library "bats-support"
bats_load_library "bats-assert"

@test 'assert_output works' {
	run echo "This test is supposed to fail, to test error reporting"
	assert_output --partial - <<< "foo"
}
